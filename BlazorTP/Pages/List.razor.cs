﻿using Blazored.LocalStorage;
using Blazored.Modal;
using Blazored.Modal.Services;
using Blazorise.DataGrid;
using BlazorTP.Modals;
using BlazorTP.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;

namespace BlazorTP.Pages
{
    public partial class List
    {
        private List<Item> items;

        private int totalItem;

        [Inject]
        public NavigationManager NavigationManager { get; set; }

        [CascadingParameter]
        public IModalService Modal { get; set; }

        [Inject]
        public HttpClient Http { get; set; }

        ///
        /// [Inject]
        /// public ILocalStorageService LocalStorage { get; set; }
        /// 

        [Inject]
        public IDataService DataService { get; set; }

        [Inject]
        public IWebHostEnvironment WebHostEnvironment { get; set; }

        [Inject]
        public IStringLocalizer<List> Localizer { get; set; }

        ///
        /// protected override async Task OnAfterRenderAsync(bool firstRender)
        ///{
        ///  // Do not treat this action if is not the first render
        ///    if (!firstRender)
        ///    {
        ///        return;
        ///    }
        ///    
        ///    var currentData = await LocalStorage.GetItemAsync<Item[]>("data");
        ///
        ///    // Check if data exist in the local storage
        ///    if (currentData == null)
        ///    {
        ///        // this code add in the local storage the fake data (we load the data sync for initialize the data before load the OnReadData method)
        ///        var originalData = Http.GetFromJsonAsync<Item[]>($"{NavigationManager.BaseUri}fake-data.json").Result;
        ///        await LocalStorage.SetItemAsync("data", originalData);
        ///    }
        ///}
        ///

        private async Task OnReadData(DataGridReadDataEventArgs<Item> e)
        {
            if (e.CancellationToken.IsCancellationRequested)
            {
                return;
            }

            // When you use a real API, we use this follow code
            //var response = await Http.GetJsonAsync<Data[]>( $"http://my-api/api/data?page={e.Page}&pageSize={e.PageSize}" );
            ///
            /// var response = (await LocalStorage.GetItemAsync<Item[]>("data")).Skip((e.Page - 1) * e.PageSize).Take(e.PageSize).ToList();
            /// 

            if (!e.CancellationToken.IsCancellationRequested)
            {
                ///
                /// totalItem = (await LocalStorage.GetItemAsync<List<Item>>("data")).Count;
                /// items = new List<Item>(response); // an actual data for the current page
                /// 

                items = await DataService.List(e.Page, e.PageSize);
                totalItem = await DataService.Count();
            }
        }

        private async void OnDelete(int id)
        {
            var parameters = new ModalParameters();
            parameters.Add(nameof(Item.Id), id);

            var modal = Modal.Show<DeleteConfirmation>("Delete Confirmation", parameters);
            var result = await modal.Result;

            if (result.Cancelled)
            {
                return;
            }

            await DataService.Delete(id);

            // Reload the page
            NavigationManager.NavigateTo("list", true);
        }
    }
}
